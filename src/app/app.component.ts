import { Component, ViewChild } from '@angular/core';
import { CKEditor4 } from 'ckeditor4-angular/ckeditor';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  @ViewChild('editor', { static: false }) editor: any;
  title = 'custom-ckeditor';
  public Config = {
    extraPlugins : 'text-field,timestamp',
    // removeButtons: 'Underline',
    // removePlugins: 'forms',
    // removeDialogTabs: 'table:advanced;image:Link'
  };
  editorUrl = '/assets/ckeditor/ckeditor.js';
  data = ''
    // '<p><input checked="checked" name="test" required="" type="checkbox" value="1" /><input name="test" type="text" value="test" /></p>';
  constructor() {}

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    
  }

  onChange(event: any) {
    // console.log(event);
    // console.log(this.data);
    // console.log(this.editor.data);
  }
}

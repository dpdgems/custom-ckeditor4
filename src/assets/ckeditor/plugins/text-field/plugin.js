/**
 * Copyright (c) 2014-2022, CKSource Holding sp. z o.o. All rights reserved.
 * Licensed under the terms of the MIT License (see LICENSE.md).
 *
 * Basic sample plugin inserting current date and time into the CKEditor editing area.
 *
 * Created out of the CKEditor Plugin SDK:
 * https://ckeditor.com/docs/ckeditor4/latest/guide/plugin_sdk_intro.html
 */

// Register the plugin within the editor.
CKEDITOR.plugins.add("text-field", {
  // Register the icons. They must match command names.
  icons: "text-field",

  // The plugin initialization logic goes inside this method.
  init: function (editor) {
    console.log("editor", editor);
    // Define the editor command that inserts a timestamp.
    editor.addCommand("insertCheck", {
      // Define the function that will be fired when the command is executed.
      exec: function (editor) {
        // Insert the timestamp into the document.
        // var input = document.createElement("input");
        // input.type = "checkbox";
        // input.name = "editor11";
        // input.className = "ckeditor";
        var id = "id" + Math.random().toString(16).slice(2)
        var element = CKEDITOR.dom.element.createFromHtml(`<input type="text" id=${id}>`);
        CKEDITOR.instances.editor1.insertElement(element);
        // document.getElementById("cke_1_contents").appendChild(input);
      },
    });

    // Create the toolbar button that executes the above command.
    editor.ui.addButton("text-field", {
      label: "Insert text-field",
      command: "insertCheck",
      toolbar: "insert",
    });
  },
});
